

#ifndef _CORE_H
#define _CORE_H

#include <stdint.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "freertos/stream_buffer.h"

#include "sys/dirent.h"

#include "esp_err.h"
#include "esp_event.h"
#include "esp_log.h"
#include "esp_spiffs.h"
#include "esp_system.h"

#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#define DELAY(ms) vTaskDelay(pdMS_TO_TICKS(ms))
#define DELAY_1S  DELAY(1000)
#define VIRC_CORE_ID_LEN (6)

//bool spiffs_init(void);
//void spiffs_usage(void);
//bool nvs_init(void);

void virc_core_init(void);
void virc_core_reset(void);
uint8_t *virc_core_id(void);
char *virc_core_id_string(void);

int64_t virc_millis(void);
bool virc_timer_has(int64_t *t, int64_t ms);
void virc_timer_reset(int64_t *t);


#endif
