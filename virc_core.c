
#include "virc_core.h"

static const char* TAG = "mog-core";

static uint8_t mac_address[VIRC_CORE_ID_LEN];
static char mac_address_string[13];


static bool
address_init
(void)
{
  esp_err_t err;
  err = esp_efuse_mac_get_default(mac_address);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_base_mac_addr_get (%s)", esp_err_to_name(err));
    return false;
  }
  sprintf(mac_address_string, "%02x%02x%02x%02X%02X%02X",
      mac_address[0], mac_address[1],
      mac_address[2], mac_address[3],
      mac_address[4], mac_address[5]);
  ESP_LOGI(TAG, "mac address: %s", mac_address_string);
  return true;
}

bool
nvs_init
(void)
{
  ESP_LOGD(TAG, "init nvs");
  esp_err_t err;
  err = nvs_flash_init();
  if (err == ESP_ERR_NVS_NO_FREE_PAGES
      || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_LOGD(TAG, "need to erase nvs (%s)", esp_err_to_name(err));
    err = nvs_flash_erase();
    if (err != ESP_OK) {
      ESP_LOGE(TAG, "failed nvs_flash_erase (%s)", esp_err_to_name(err));
      return false;
    }
    err = nvs_flash_init();
  }
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed nvs_flash_init (%s)", esp_err_to_name(err));
    return false;
  }
  ESP_LOGD(TAG, "init nvs complete");
  return true;
}

bool
spiffs_init
(void)
{
  ESP_LOGD(TAG, "init spiffs");

  esp_err_t err;

  // Pre-loaded assets directory
  esp_vfs_spiffs_conf_t conf_assets = {
    .base_path = "/assets",
    .partition_label = "assets",
    .max_files = 15,
    .format_if_mount_failed = true
  };

  err = esp_vfs_spiffs_register(&conf_assets);

  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_vfs_spiffs_register (%s)", esp_err_to_name(err));
    return false;
  }

  // Storage partition
  esp_vfs_spiffs_conf_t conf_storage = {
    .base_path = "/storage",
    .partition_label = "storage",
    .max_files = 15,
    .format_if_mount_failed = true
  };

  err = esp_vfs_spiffs_register(&conf_storage);

  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_vfs_spiffs_register (%s)", esp_err_to_name(err));
    return false;
  }

  ESP_LOGD(TAG, "init spiffs complete");
  return true;
}

void
spiffs_usage
(void)
{
  size_t total = 0;
  size_t usage = 0;
  esp_err_t err;
  err  = esp_spiffs_info("assets", &total, &usage);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed to get fs info: (%s)", esp_err_to_name(err));
    return;
  }
  ESP_LOGD(TAG, "/assets info: total=%d, used=%d", total, usage);

  //esp_spiffs_format("storage");
  err  = esp_spiffs_info("storage", &total, &usage);
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed to get fs info: (%s)", esp_err_to_name(err));
    return;
  }
  ESP_LOGD(TAG, "/storage info: total=%d, used=%d", total, usage);

  DIR *dir = opendir("/storage");
  if (!dir) {
    ESP_LOGE(TAG, "error opening /storage");
    return;
  }
  struct dirent *ent;
  while ((ent = readdir(dir)) != NULL) {
    ESP_LOGI(TAG, "/storage/%s", ent->d_name);
  }

}

int64_t
virc_micros
(void)
{
  struct timeval tv_now;
  gettimeofday(&tv_now, NULL);
  return (int64_t)tv_now.tv_sec * 1000000L + (int64_t)tv_now.tv_usec;
}

int64_t
virc_millis
(void)
{
  return virc_micros() / 1000;
}


void
virc_timer_reset
(int64_t *t)
{
  *t = virc_millis();
}

int64_t
virc_timer_delta
(int64_t *t)
{
  return virc_millis() - *t;
}

bool
virc_timer_has(int64_t *t, int64_t ms) {
  bool has = virc_timer_delta(t) > ms;
  if (has) {
    virc_timer_reset(t);
  }
  return has;
}


void
virc_core_reset
(void)
{
  esp_err_t err;
  err = esp_spiffs_format("storage");
  if (err != ESP_OK) {
    ESP_LOGE(TAG, "failed esp_spiffs_format (%s)", esp_err_to_name(err));
  }
  esp_restart();
}

uint8_t*
virc_core_id
(void)
{
  return mac_address;
}

char*
virc_core_id_string
(void)
{
  return mac_address_string;
}

static void
logs_init
(void)
{
  // man this thing is noisy
  esp_log_level_set("phy", ESP_LOG_WARN);
  esp_log_level_set("wifi", ESP_LOG_ERROR);
  esp_log_level_set("wifi_init", ESP_LOG_WARN);
  esp_log_level_set("system_api", ESP_LOG_WARN);
  esp_log_level_set("BTDM_INIT", ESP_LOG_WARN);
  esp_log_level_set("esp_netif_handlers", ESP_LOG_WARN);
}


void
virc_core_init
(void)
{
  logs_init();
  nvs_init();
  address_init();
  spiffs_init();
  spiffs_usage();
}
